module Main where

import Trouble

main :: IO ()
main = do
  putStr "Build a rocket... \n"
  print $ rocket [(120,2),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
  print $ orbiters [Rocket 300 [Rocket 200 [Cargo [Orbiter "LRO", Probe "Lunokhod"]], Cargo [Orbiter "ISS"]]]
  print $ finalFrontier [IsDead "He", Fascinating, Warp 3, LiveLongAndProsper]